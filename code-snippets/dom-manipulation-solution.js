export default `\
new Popper(referenceElement, popoverElement, {
  modifiers: {
    applyStyle: { fn: updateComponentState },
  },
});

// React component method
const updateComponentState = (data) => {
  this.setState({
    offsets: data.offsets.popper,
  });
  return data;
}
\
`;

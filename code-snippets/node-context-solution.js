export default `\
<body>

<main>
  <section>
    <button>
      I have a popover attached 👇
    </button>
  </section>
  <div style="...">
    I'm the popover 👋
  </div>
</main>

</body>
\
`;

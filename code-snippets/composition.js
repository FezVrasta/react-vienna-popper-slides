export default `\
const Tip = ({ close, isOpen, children }) =>
 <Manager>
  <ClickOutside onClickOutside={close}>
    <Target>👇</Target>
    <Portal>
      {isOpen && <Popper>
        {({ scheduleUpdate, ...props }) =>
          <ResizeAware
            onResize={scheduleUpdate}
            {...props}
          >
            {children} 👋
          </ResizeAware>
        }
      </Popper>}
    </Portal>
   </ClickOutside>
 </Manager>\
`;

export default `\
<body>

<main>
  <section>
    <button>
      I have a popover attached 👇
    </button>
  </section>
  <!-- popover was here -->
</main>
<div style="...">
  I'm the popover 👋
</div>

</body>
\
`;

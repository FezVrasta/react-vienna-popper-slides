// Import React
import React, { Component } from "react";

import FontAwesome from "react-fontawesome";

// Import Spectacle Core tags
import {
  Code,
  Appear,
  Image,
  S,
  Deck,
  Heading,
  ListItem,
  List,
  Slide,
  Text,
  BlockQuote,
  Quote,
  Cite
} from "spectacle";
import CodeSlide from "spectacle-code-slide";
import ImageSlide from "spectacle-image-slide";

// Import image preloader util
import preloader from "spectacle/lib/utils/preloader";

// Import theme
import createTheme from "spectacle/lib/themes/default";

// Demo components
import { Manager, Target, Popper } from "react-popper";
import ClickOutside from "react-click-outside";
import ResizeAware from "react-resize-aware";

// Code snippets
const snippets = {
  domManipulationProblem: require("../code-snippets/dom-manipulation-problem.js")
    .default,
  nodeContextProblem: require("../code-snippets/node-context-problem.js")
    .default,
  domManipulationSolution: require("../code-snippets/dom-manipulation-solution.js")
    .default,
  nodeContextSolution: require("../code-snippets/node-context-solution.js")
    .default,
  composition: require("../code-snippets/composition.js").default
};

// Require CSS
require("normalize.css");
require("spectacle/lib/themes/default/index.css");
require("../assets/style.css");

const images = {
  avatar: require("../assets/avatar.png"),
  quid: require("../assets/quid.svg"),
  homerTelephone: require("../assets/homerTelephone.gif"),
  demoTime: require("../assets/demotime.gif"),
  what: require("../assets/whatisapopper.svg")
};

preloader(images);

const theme = createTheme(
  {
    primary: "#2E3842",
    secondary: "#FFFFFF",
    tertiary: "#21B2A6",
    quartenary: "#CECECE",
    lightTeal: "#00ffea"
  },
  {
    primary: "Open Sans",
    secondary: "Open Sans"
  }
);

// eslint-disable-next-line
function randomWave() {
  const items = ["👋", "👋🏻", "👋🏼", "👋🏽", "👋🏾", "👋🏿"];
  return items[Math.floor(Math.random() * items.length)];
}

class Demo extends Component {
  state = { waves: [] };

  handleClose = () => {
    // eslint-disable-next-line
    this.setState(({ waves }) => ({ waves: waves.concat(randomWave()) }));
  };

  render() {
    return (
      <Manager
        id="boundaries"
        style={{
          position: "relative",
          fontSize: "2em",
          userSelect: "none",
          width: "100%",
          backgroundColor: "rgba(0, 0, 0, .2)",
          borderRadius: 10,
          height: 570
        }}
      >
        <Target
          style={{
            position: "absolute",
            top: "60%",
            left: "50%",
            transform: "translateX(-50%)"
          }}
        >
          {this.state.waves.length < 4 ? "👇" : "👆"}
        </Target>
        <ClickOutside onClickOutside={this.handleClose}>
          <Popper
            modifiers={{
              preventOverflow: {
                boundariesElement: document.querySelector("#boundaries")
              }
            }}
          >
            {({ popperProps }) => (
              <ResizeAware>
                <div
                  {...popperProps}
                  style={{
                    ...popperProps.style,
                    position: "absolute",
                    backgroundColor: "#FFC107",
                    padding: ".2em",
                    borderRadius: 3,
                    boxShadow: "0 0 2px rgba(0,0,0,0.5)",
                    color: "#000000",
                    maxWidth: 300
                  }}
                >
                  {this.state.waves.slice(0, 6).join(" ") || "🇦🇹"}
                </div>
              </ResizeAware>
            )}
          </Popper>
        </ClickOutside>
      </Manager>
    );
  }
}

// eslint-disable-next-line
const onClose = () => console.log('close');

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck
        transition={["zoom", "slide"]}
        transitionDuration={500}
        theme={theme}
      >
        <Slide
          transition={["zoom"]}
          bgColor="primary"
          style={{
            backgroundImage: "linear-gradient(to right, #304f56 0%, #304f56 12.5%, #1c3e43 12.5%, #1c3e43 25%, #304e29 25%, #304e29 37.5%, #4f561c 37.5%, #4f561c 50%, #625618 50%, #625618 62.5%, #613814 62.5%, #613814 75%, #5d2a41 75%, #5d2a41 87.5%, #4c2742 87.5%)"
          }}
        >
          <Heading
            size={1}
            caps
            lineHeight={1}
            textColor="secondary"
            style={{
              borderWidth: ".05em 0",
              borderStyle: "solid",
              padding: ".4em",
              display: "inline-block"
            }}
          >
            Popper.js
          </Heading>

          <Heading size={2} fit caps lineHeight={3} textColor="secondary">
            Positioning libraries integration in React
          </Heading>
        </Slide>
        <Slide transition={["zoom", "fade"]} bgColor="primary">
          <Image src={images.avatar} width={180} />
          <Heading size={3} caps bold lineHeight={1} textColor="secondary">
            Federico Zivolo
          </Heading>
          <Text textColor="tertiary" lineHeight={2} size={1}>
            <FontAwesome name="twitter" />
            {" "}
            @FezVr4sta
            &nbsp;&nbsp;&nbsp;&nbsp;
            <FontAwesome name="github" />
            {" "}
            FezVrasta
          </Text>
          <Text textColor="quartenary" size={1}>
            UI Specialist at
            {" "}
            <Image
              src={images.quid}
              style={{ position: "relative", top: ".7em", opacity: 0.73 }}
              height={66}
            />
          </Text>
        </Slide>
        <Slide transition={["fade"]} bgColor="primary">
          <Heading size={3} textColor="quartenary" lineHeight={2}>
            Positioning Libraries?
          </Heading>
          <Text textColor="terziary" textSize="1.5em">
            Library for making an element (popper) stay near to another element (reference).
          </Text>
          <Appear><Text textSize="4em" lineHeight={2}>🤔</Text></Appear>
        </Slide>
        <Slide transition={["fade"]} bgColor="primary">
          <Image src={images.what} />
          <Appear>
            <Text textSize="3em" lineHeight={1}>
              💡<br />😃
            </Text>
          </Appear>
        </Slide>
        <Slide transition={["fade"]} bgColor="primary">
          <Heading size={1} fit caps lineHeight={1} textColor="quartenary">
            The problem(s)
          </Heading>
          <List>
            <Appear><ListItem>Communication between parties</ListItem></Appear>
            <Appear>
              <ListItem>DOM manipulated by 3rd party libraries</ListItem>
            </Appear>
            <Appear><ListItem>Node context is meaningful</ListItem></Appear>
          </List>
        </Slide>
        <Slide
          transition={["fade"]}
          bgColor="primary"
          notes={
            <div>
              <ol>
                <li>
                  Know when the popover is positioned to show it, run callbacks, anything
                </li>
                <li>
                  Be able to interfere with the library to modifiy its behavior or
                  your React component.
                </li>
                <li>
                  Get all the possible data to delegate to React as much as possible
                </li>
              </ol>
            </div>
          }
        >
          <ImageSlide
            title="Communication between parties"
            titleProps={{ size: 3, fit: false }}
            image={images.homerTelephone}
          />
        </Slide>

        <CodeSlide
          transition={["fade"]}
          lang="html"
          code={snippets.domManipulationProblem}
          bgColor="primary"
          notes={
            <div>
              <ol>
                <li>
                  Edit inline style directly, may get overridden by other code
                </li>
                <li>
                  Doesn't follow React lifecycle
                </li>
              </ol>
            </div>
          }
          ranges={[
            {
              loc: [0],
              title: (
                <Heading size={6} caps lineHeight={1} textColor="quartenary">
                  DOM manipulated by 3rd party libraries
                </Heading>
              )
            },
            { loc: [2, 3] },
            { loc: [3, 8] }
          ]}
        />
        <CodeSlide
          transition={["fade"]}
          lang="html"
          code={snippets.nodeContextProblem}
          bgColor="primary"
          ranges={[
            {
              loc: [0],
              title: (
                <Heading size={6} caps lineHeight={1} textColor="quartenary">
                  Node context is meaningful
                </Heading>
              )
            },
            { loc: [4, 7] },
            { loc: [2, 10] },
            { loc: [10, 13] }
          ]}
          notes={
            <div>
              <ol>
                <li>
                  Moves popper to different place in DOM three
                </li>
                <li>
                  Accessibility practices in place: keyboard navigation AND screen readers
                </li>
                <li>
                  React may lose track of where the node is
                </li>
                <li>
                  Scrollable or transformed parent
                </li>
              </ol>
            </div>
          }
        />
        <Slide transition={["fade"]} bgColor="primary">
          <Heading size={1} fit caps lineHeight={1} textColor="quartenary">
            Solution<S type="strikethrough">(s)</S>
          </Heading>
          <List>
            <Appear>
              <ListItem>Middlewares and lifecycle hooks</ListItem>
            </Appear>
            <Appear>
              <ListItem>Delegate DOM manipulation to React</ListItem>
            </Appear>
            <Appear><ListItem>No node context manipulation</ListItem></Appear>
          </List>
        </Slide>
        <Slide
          transition={["fade"]}
          bgColor="primary"
          notes={
            <div>
              <ol>
                <li>
                  Foundamentals of Popper.js infrastructure
                </li>
                <li>
                  Modifiers make you inject and override any logic
                </li>
                <li>
                  Both allow to extract data from Popper.js
                </li>
              </ol>
            </div>
          }
        >
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            Middlewares and lifecycle hooks
          </Heading>
          <List>
            <Appear>
              <ListItem><Code textColor="ternary">onCreate</Code></ListItem>
            </Appear>
            <Appear>
              <ListItem><Code textColor="ternary">onUpdate</Code></ListItem>
            </Appear>
            <Appear><ListItem><S type="italic">modifiers</S></ListItem></Appear>
          </List>
        </Slide>
        <CodeSlide
          transition={["fade"]}
          lang="js"
          code={snippets.domManipulationSolution}
          bgColor="primary"
          ranges={[
            {
              loc: [0],
              title: (
                <Heading size={6} caps lineHeight={1} textColor="quartenary">
                  Delegate DOM manipulation to React
                </Heading>
              )
            },
            {
              loc: [2, 3],
              note: "Override built-in DOM manipulation function"
            },
            { loc: [6, 13], note: "Update React component state" }
          ]}
        />
        <CodeSlide
          transition={["fade"]}
          lang="html"
          code={snippets.nodeContextSolution}
          bgColor="primary"
          ranges={[
            {
              loc: [0],
              title: (
                <Heading size={6} caps lineHeight={1} textColor="quartenary">
                  No node context manipulation
                </Heading>
              )
            },
            { loc: [4, 7] },
            { loc: [8, 11] },
            { loc: [2, 12] }
          ]}
        />
        <Slide transition={["fade"]} bgColor="primary">
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            Bonus point, composition ♥️
          </Heading>
          <List>
            <ListItem>React Popper</ListItem>
            <List>
              <ListItem>+= React Portal</ListItem>
              <ListItem>+= React Resize Aware</ListItem>
              <ListItem>+= React Click Outside</ListItem>
              <ListItem>
                += React <S type="italic">&lt;insert catchy name here&gt;</S>
              </ListItem>
            </List>
          </List>
        </Slide>
        <CodeSlide
          transition={["fade"]}
          lang="jsx"
          code={snippets.composition}
          bgColor="primary"
          ranges={[
            { loc: [0, 12] },
            { loc: [1, 2], note: "import { Manager } from 'react-popper'" },
            {
              loc: [2, 3],
              note: "import ClickOutside from 'tj/react-click-outside'"
            },
            { loc: [3, 4], note: "import { Target } from 'react-popper'" },
            { loc: [4, 5], note: "import Portal from 'react-portal'" },
            { loc: [5, 7], note: "import { Popper } from 'react-popper'" },
            {
              loc: [7, 12],
              note: "import ResizeAware from 'react-resize-aware'"
            }
          ]}
        />
        <Slide transition={["fade"]} bgColor="primary">
          <ImageSlide title="Demo time" image={images.demoTime} />
        </Slide>

        <Slide transition={["fade"]} bgColor="primary">
          <Demo close={onClose} />
        </Slide>

        <Slide transition={["fade"]} bgColor="primary">
          <Heading size={3} textColor="quartenary" lineHeight={2}>
            Questions?
          </Heading>
        </Slide>
      </Deck>
    );
  }
}
